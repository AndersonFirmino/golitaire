#tool
extends Position2D

export var suit = 0 setget set_suit 
export var card = 0 setget set_card 
export var face_up = false setget set_face  
export var place = false setget set_place	
export var offset_pos = Vector2(0,30) setget set_offset	
export var isDeck = false				
export var ruleID = 0

var main_node
var picked = false							
var mouse_pos_offset = Vector2()			
var snapped = null							
var placedOn = null							
var hColor = Color(0.6, 1, 0.6)				
var dColor = Color(1, 1, 1)					
var unfoldTo = null							
var lastPressTime = 0.0

func _ready():
	main_node = get_node("/root/main")
	$debug/Label.text = name
	if face_up or main_node.allowe_all:
		main_node.addToList(self)
	card_update()
	set_physics_process(false)
	set_process(false)
	

func _process(delta):
# Debug -----------------------------
	if placedOn:
		$debug/Label2.text = placedOn.name
	else:
		$debug/Label2.text = ""
	if picked:
		$debug/np.modulate = hColor
	else:
		$debug/np.modulate = dColor
	if unfoldTo:
		$debug/np2.modulate = hColor
	else:
		$debug/np2.modulate = dColor
	if snapped:
		$debug/Label3.text = "s_" + snapped.name
		$debug/Label3.modulate = Color(1,1,1,1)
	else:
		$debug/Label3.text = ""
		$debug/Label3.modulate = Color(1,1,1,0)
	$debug/Label4.text = str(ruleID)
# Debug -----------------------------

func save():
	var _snapped = null
	if snapped:
		_snapped = snapped.save()
	
	var save_dict = {
		name=self.name,
		suit=suit,
		card=card,
		face_up=face_up,
		place=place,
		snapped=_snapped,
		unfoldTo=unfoldTo
	}
	return save_dict

func set_face(value):
	face_up = value
	card_update()

func set_suit(value):		
	suit = value
	card_update()			

func set_card(value):		
	card = value
	card_update()				

func set_place(value):
	place = value
	card_update()

func getOffsetPos():
	if place:
		return global_position
	if has_node("offset"):
		return $offset.global_position
	return global_position
	
func set_offset(value):
	offset_pos = value
	if has_node("offset"):
		$offset.position = offset_pos

func card_update():			
	if has_node("card"):
		$card.visible = !place 
		$empty.visible = place 

		if !face_up:
			$card.frame = 53
		else:
			$card.frame = card + 13 * suit
	if has_node("Label"):
		$Label.text = name

func _on_Button_button_down():
	if (OS.get_ticks_msec() - lastPressTime) < 200:
		main_node.autoMove(self)
		return
	lastPressTime = OS.get_ticks_msec()
	if place:
		if isDeck:
			main_node.allowe_all = true
			var _un = main_node.get_node("unfold")
			if _un.snapped:
				main_node.flip_from(_un.snapped, self)
				_un.snapped = null
				main_node.addToList(_un)
			main_node.allowe_all = false
		return
	if !face_up:
		if !snapped:
			face_up = true
			card_update()
			if unfoldTo:
				main_node.allowe_all = true
				main_node.pickedCards = []
#				main_node.change_h(self, main_node)
				global_position = unfoldTo.global_position
				if placedOn:
					placedOn.snapped = null
					placedOn = null
				find_place()
				main_node.allowe_all = false
			main_node.addToList(self)
		return
	if placedOn:
		placedOn.snapped = null
		main_node.addToList(placedOn)
	mouse_pos_offset = global_position - get_global_mouse_position() 
	picked = true
	main_node.change_h(self, main_node)
	main_node.pickedCards = []
	main_node.selectedCard = self
	addToTockedList()
	set_physics_process(true)

func set_card_from_id(id):		
	suit = id / 13
	card = id - suit * 13
	card_update()

func addToTockedList():
	main_node.pickedCards.append(self)
	if (snapped != null):
		snapped.addToTockedList()

func openCard(value):
	face_up = value
	card_update()

func flipCard():
	face_up = !face_up
	card_update()
	if !face_up:
		main_node.delFromList(self)
	else:
		main_node.addToList(self)

func _input(event):
	if Input.is_action_just_released("LMB") and picked:
		find_place()

func find_place():
	set_physics_process(false)
	picked = false
	var c = main_node.findClosest(self)
	if c:
		snap_to(c)
	elif placedOn and !main_node.allowe_on_table:
		snap_to(placedOn)
		pass
	main_node.clearLast()

func snap_to(_card):
	if _card != placedOn and placedOn != null and !unfoldTo:
		placedOn.openCard(true)
	var n = name
	main_node.change_h(self, _card)
	main_node.delFromList(_card)
	_card.snapped = self
	placedOn = _card
	correct_offset()
	name = n 
	ruleID = _card.ruleID
	if main_node.allowe_victory:
		main_node.checkVictory()
		main_node.ifAllOpened()
		
func recursive_open(_open):
	openCard(_open)
	if snapped:
		snapped.recursive_open(_open)

func correct_offset():
	if !get_parent():
		return
	var parent_card = get_parent()
	if has_node("offset") and parent_card.has_node("offset"):
		$offset.position = parent_card.get_node("offset").position
		global_position = parent_card.getOffsetPos()
	if snapped:
		snapped.correct_offset()

func highlight(value):
	if value:
		$card.modulate = hColor
		$empty.modulate = hColor
	else:
		$card.modulate = dColor
		$empty.modulate = dColor

func _physics_process(delta):
	if picked:				
		global_position = get_global_mouse_position() + mouse_pos_offset
		main_node.findClosest(self)
