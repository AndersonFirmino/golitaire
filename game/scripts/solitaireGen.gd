extends Node
var main_node
var cardObj = load("res://objects/card.tscn")
var dec_of_card = []
var global

func _ready():
	global = get_node("/root/global")
	randomize()
	main_node = get_node("/root/main")
	if global.load_from != -1:
		load_game()
		return
	for i in range(52):
		dec_of_card.append(i)
	var max_in_row = 1
	var n = 0
	for i in range(7): 
		var row = main_node.get_node("row" + str(i+1))
		var pos = row.global_position
		for j in range(max_in_row):
			var card = cardObj.instance()
			card.global_position = pos
			main_node.add_child(card)
			card.main_node = main_node
			card.find_place()
			card.set_card_from_id(random_card())
			n += 1
			if j == max_in_row - 1:
				card.openCard(true)
			pos = card.global_position
		max_in_row += 1
#
	var deck = main_node.get_node("deck")
	var unfoldTo = main_node.get_node("unfold")
	var pos = deck.global_position
	for i in range(n, 52):
		var card = cardObj.instance()
		card.global_position = pos
		main_node.add_child(card)
		card.main_node = main_node
		card.find_place()
		card.set_card_from_id(random_card())
		card.unfoldTo = unfoldTo
	main_node.remove_face_down()
	main_node.allowe_all = false
	main_node.allowe_on_table = false

	main_node.allowe_all = false
#
func random_card():
	var r = randi()%dec_of_card.size()
	var id = dec_of_card[r]
	dec_of_card.remove(r)
	return id

func load_card(data, place):
	if !data:
		return
	main_node.allowe_all  = true
	var pos = place.global_position
	var card = cardObj.instance()
	card.global_position = pos
	main_node.add_child(card)
	card.main_node = main_node
	card.find_place()
	
	card.suit = data["suit"]
	card.card = data["card"]
	card.name = data["name"]
	card.face_up = data["face_up"]
	card.place = data["place"]
	if data["unfoldTo"]:
		card.unfoldTo = main_node.get_node("unfold")
	
	main_node.allowe_on_table = false
	main_node.allowe_all = false
	if data["snapped"]:
		load_card(data["snapped"], card)
	else:
		main_node.addToList(card)
		
func load_game():
	main_node.allowe_victory = false
	var data = global.load_game()
	if data:
		for place_name in data.keys():
			var node = main_node.get_node(place_name)
			load_card(data[place_name]["snapped"], node)
	main_node.allowe_victory = true