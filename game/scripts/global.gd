extends Node
var load_from = -1
var SAVE_PATH = "res://savegame_"
var version = '0.9.0'

var fileExists = []
func _ready():
	var save_file = File.new()
	for i in range(10):
		fileExists.append(save_file.file_exists(SAVE_PATH + str(i) + ".save"))
	print(fileExists.size())

func save_game(id):
	var save_dict = {}
	var nodes_to_save = get_tree().get_nodes_in_group("place")
	for node in nodes_to_save:
		save_dict[node.name] = node.save()
		
	var save_file = File.new()
	save_file.open(SAVE_PATH + str(id) + ".save", File.WRITE) 
	
	save_file.store_line(to_json(save_dict))
	save_file.close()
	fileExists[id] = true
	
func load_game():
	var save_file = File.new()
	if !save_file.file_exists(SAVE_PATH + str(load_from) + ".save"):
		return null
	save_file.open(SAVE_PATH + str(load_from) + ".save", File.READ)
	var data = {}
	data = parse_json(save_file.get_as_text())
	return data
